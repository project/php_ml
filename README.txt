
-- SUMMARY --

Integration module with PHP-ML - Machine Learning library for PHP.

For a full description of the module, visit the project page:
  http://drupal.org/project/php_ml
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/php_ml

-- REQUIREMENTS --

* php-ml
  https://github.com/php-ai/php-ml

* PHP >= 7.1.

-- INSTALLATION --

1. Execute 'composer update' from the site's root directory (this will download
   the PHP-ML library).

2. Install the module as usual, see http://drupal.org/node/70151 for further
   information.

  -- USAGE --

* After the module configuration you can use the PHP-ML library in your custom modules.

-- CREDITS --

Authors:
* Eleo Basili (eleonel) - http://drupal.org/u/eleonel

This project has been sponsored by Spinetta (http://spinetta.tech).
